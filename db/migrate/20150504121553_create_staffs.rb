class CreateStaffs < ActiveRecord::Migration
  def change
    create_table :staffs do |t|
      t.string :name
      t.text :address
      t.string :telephone_no
      t.string :division_id

      t.timestamps
    end
  end
end
