class CreateUploads < ActiveRecord::Migration
  def change
    create_table :uploads do |t|
      t.belongs_to :asset, index:true
   	  t.string :filename
      t.string :content_type
      t.binary :file_contents


      t.timestamps
    end
  end
end
