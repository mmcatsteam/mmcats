class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.string :finance_reference_id
      t.string :name
      t.text :description
      t.text :address
      t.integer :subnumber
      t.boolean :archived
      t.string :current_holder_id
      t.string :division_id
      t.integer :quantity
      t.string :company_id
      t.string :base_unit
      t.string :verifier
      t.string :asset_manufacturer
      t.datetime :print_date
      t.datetime :tag_date
      t.datetime :last_check
      t.string :asset_type
      t.decimal :acquisition_value
      t.decimal :current_value

      t.timestamps
    end
  end
end
