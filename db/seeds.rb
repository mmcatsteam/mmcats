# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

assets = Asset.create([{ finance_reference_id: 'MMCW-410601-00083', description: 'LCD Projector', division_id: 'MCSB 06', asset_type: 'Office Equipment'}, {finance_reference_id: 'MMCW-410101-00082', description: 'Server 200.13.1.134', division_id: 'MCSB 06', asset_type: 'Office Equipment'},{finance_reference_id: '027W-410601-00001', description: 'HP Notebook 4410s', division_id: 'MCSB 06', asset_type: 'Computer & Support System'},{finance_reference_id: '027W-410601-01330', description: '(1) Bulb for Sanyo LCD Projector (PLC-SW35)', division_id: 'MCSB 06', asset_type: 'Office Equipment'},{finance_reference_id: '027W-410101-01321', description: 'PV100136 : DELL(TM) POWEREDGE(TM) T110 SERVER', division_id: 'MCSB 06', asset_type: 'Computer & Support System'}])