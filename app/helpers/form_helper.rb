module FormHelper
  def setup_asset(asset)
    asset.upload ||= Upload.new
    asset
  end
end