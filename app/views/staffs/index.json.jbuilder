json.array!(@staffs) do |staff|
  json.extract! staff, :id, :\, :name, :address, :telephone_no, :division_id
  json.url staff_url(staff, format: :json)
end
