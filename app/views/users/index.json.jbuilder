json.array!(@users) do |user|
  json.extract! user, :id, :/, :name, :profile_pic_id, :address, :telephone_no, :rank_id, :division_id
  json.url user_url(user, format: :json)
end
