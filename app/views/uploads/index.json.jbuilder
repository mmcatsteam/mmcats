json.array!(@attachments) do |attachment|
  json.extract! attachment, :id, :/, :entity_id, :entity_type, :file_url, :description
  json.url attachment_url(attachment, format: :json)
end
