json.array!(@assets) do |asset|
  json.extract! asset, :id, :\, :finance_reference_id, :name, :description, :address, :subnumber, :archived, :current_holder_id, :division_id, :quantity, :company_id, :base_unit, :verifier, :asset_manufacturer, :print_date, :tag_date, :last_check, :asset_type, :acquisition_value, :current_value
  json.url asset_url(asset, format: :json)
end
