class AssetsController < ApplicationController
    before_filter :authenticate_user!, :except => [:show, :index]
    before_action :set_asset, only: [:show, :edit, :update, :destroy]
    helper_method :sort_column, :sort_direction


  # GET /assets
  # GET /assets.json
  def index
        @assets = Asset.search(params[:search]).order(sort_column + " " + sort_direction).where(:archived => false).paginate(:per_page => 5, :page => params[:page])
  end



  # GET /assets/1
  # GET /assets/1.json
  def show
  end

  # GET /assets/new
  def new
    @asset = Asset.new
    @asset.uploads.build
  end


  # GET /assets/1/edit
  def edit
  end

  def disposed
    @assets = Asset.search(params[:search]).order(sort_column + " " + sort_direction).where(:archived => true).paginate(:per_page => 5, :page => params[:page])
    render action: :index
  end

  def archived

    @asset = Asset.find(params[:id])
    @asset.toggle!(:archived)

     respond_to do |format|
        if @asset.save
          format.html { redirect_to assets_url, notice: 'Asset archived' }
          format.js
        else
          format.html { redirect_to assets_url, notice: 'Unable to archived asset' }
          format.js
        end
     end
  end


  # POST /assets
  # POST /assets.json
  def create
    @asset = Asset.new(asset_params)
    respond_to do |format|
      if @asset.save
        format.html { redirect_to @asset, notice: 'Asset was successfully created.' }
        format.json { render :show, status: :created, location: @asset }
      else
        format.html { render :new }
        format.json { render json: @asset.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assets/1
  # PATCH/PUT /assets/1.json
  def update
    respond_to do |format|
      if @asset.update(asset_params)
        format.html { redirect_to @asset, notice: 'Asset was successfully updated.' }
        format.json { render :show, status: :ok, location: @asset }
      else
        format.html { render :edit }
        format.json { render json: @asset.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assets/1
  # DELETE /assets/1.json
  def destroy
    @asset.destroy
    respond_to do |format|
      format.html { redirect_to assets_url, notice: 'Asset was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_asset
      @asset = Asset.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def asset_params
      params.require(:asset).permit(:id, :finance_reference_id, :name, :description, :address, :subnumber, :archived, :current_holder_id, :division_id, :quantity, :company_id, :base_unit, :verifier, :asset_manufacturer, :print_date, :tag_date, :last_check, :asset_type, :acquisition_value, :current_value, { uploads_attributes: [ :file ]})
    end

    def sort_column
      Asset.column_names.include?(params[:sort]) ? params[:sort] : "name"
    end
  
    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end

end
