class Asset < ActiveRecord::Base
    has_many :uploads, :dependent => :destroy
    belongs_to :division

    accepts_nested_attributes_for :uploads,
        :allow_destroy => true,
        :reject_if     => :all_blank


    validates :finance_reference_id,:address,:subnumber,:division_id,:quantity,:company_id,:base_unit,
    :verifier,:asset_manufacturer,:print_date,:tag_date,:last_check,:asset_type,:acquisition_value,:current_value, presence: true
    
    validates :id, :subnumber,:acquisition_value,:current_value, numericality: true

    validates :id, uniqueness: :true

    def self.search(search)
        if search
             where('name  LIKE ?', "%#{search}%")
         else
             all
        end
    end

    
    
end


#t.string :name
    # t.text :description
     # t.text :address
     # t.integer :subnumber
     # t.boolean :archived
     # t.string :current_holder_id
     # t.string :division_id
     # t.integer :quantity
     # t.string :company_id
     # t.string :base_unit
     # t.string :verifier
     # t.string :asset_manufacturer
     # t.datetime :print_date
     # t.datetime :tag_date
     # t.datetime :last_check
     # t.string :asset_type
    #  t.decimal :acquisition_value
     # t.decimal :current_value