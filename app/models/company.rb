class Company < ActiveRecord::Base
    has_many :asset
    validates :id, :name, :description, presence: true
    validates :name, uniqueness:true
    validates :id, uniqueness: :true

    	def self.search(search)
        	if search
            	 where('name LIKE ?', "%#{search}%")
         	else
            	 all
        	end
    	end

end



# t.string :name
     # t.text :description