class Staff < ActiveRecord::Base
	has_one :asset, foreign_key: 'verifier'
	belongs_to :division
	
	validates :id, :name, :address, :telephone_no,:division_id, presence:true	
	validates :id, uniqueness:true

		def self.search(search)
        	if search
            	 where('name LIKE ?', "%#{search}%")
         	else
            	 all
        	end
    	end
	
end
