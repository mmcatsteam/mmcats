class Division < ActiveRecord::Base
  	has_many :asset, dependent: :destroy
  	has_many :staff
    validates :id, :name, :description, presence: true
    validates :id, :name, uniqueness: true
    validates :id, numericality: true

    
    	def self.search(search)
        	if search
            	 where('name LIKE ?', "%#{search}%")
         	else
            	 all
        	end
    	end

end



 #t.string :name
    #  t.text :description