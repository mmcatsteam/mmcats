require 'test_helper'

class AssetsControllerTest < ActionController::TestCase
  setup do
    @asset = assets(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:assets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create asset" do
    assert_difference('Asset.count') do
      post :create, asset: { \: @asset.\, acquisition_value: @asset.acquisition_value, address: @asset.address, archived: @asset.archived, asset_manufacturer: @asset.asset_manufacturer, asset_type: @asset.asset_type, base_unit: @asset.base_unit, company_id: @asset.company_id, current_holder_id: @asset.current_holder_id, current_value: @asset.current_value, description: @asset.description, division_id: @asset.division_id, finance_reference_id: @asset.finance_reference_id, last_check: @asset.last_check, name: @asset.name, print_date: @asset.print_date, quantity: @asset.quantity, subnumber: @asset.subnumber, tag_date: @asset.tag_date, verifier: @asset.verifier }
    end

    assert_redirected_to asset_path(assigns(:asset))
  end

  test "should show asset" do
    get :show, id: @asset
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @asset
    assert_response :success
  end

  test "should update asset" do
    patch :update, id: @asset, asset: { \: @asset.\, acquisition_value: @asset.acquisition_value, address: @asset.address, archived: @asset.archived, asset_manufacturer: @asset.asset_manufacturer, asset_type: @asset.asset_type, base_unit: @asset.base_unit, company_id: @asset.company_id, current_holder_id: @asset.current_holder_id, current_value: @asset.current_value, description: @asset.description, division_id: @asset.division_id, finance_reference_id: @asset.finance_reference_id, last_check: @asset.last_check, name: @asset.name, print_date: @asset.print_date, quantity: @asset.quantity, subnumber: @asset.subnumber, tag_date: @asset.tag_date, verifier: @asset.verifier }
    assert_redirected_to asset_path(assigns(:asset))
  end

  test "should destroy asset" do
    assert_difference('Asset.count', -1) do
      delete :destroy, id: @asset
    end

    assert_redirected_to assets_path
  end
end
